# pinetta
*A FOSS, federated, ActivityPub-enabled pinboard thingy, in the style of Pinterest.*

Join us on Matrix at https://matrix.to/#/#pinetta:matrix.org

![Chat](https://img.shields.io/matrix/pinetta:matrix.org)  ![Follow](https://img.shields.io/mastodon/follow/109386276818343749?domain=https%3A%2F%2Ffosstodon.org&style=social)

## Overview

Pinetta (under development) is a federated social pinboard in the style of Pinterest. It allows users to pin, organize, display and share items and collections of items, including text and audio-visual media. Individual users can comment on each others' pins and collaborate with each other on joint collections.

***See also:** [Prototype/alpha version](https://codeberg.org/pinetta/proto-pinetta)*

## Features

**Posts and comments**
- Many post types: text, links, images/GIFs, video, audio
- Comments on all post types, displayed as threaded comments below posts
- Markdown syntax for descriptions and comments
  - incl. checkboxes for checklists (e.g. recipes or multi-step instructions)

**Feeds, accounts, hashtags, boards**
- Home, local, and federated feeds
  - Display modes: feed, grid, masonry
- Ability to view and follow accounts, hashtags, and boards
  - Several different display modes: feed, grid, masonry
  - Boards also include a scrolling pinboard (or "mood board") mode which allows users to arrange media manually on a canvas

**Federation**
- ActivityPub federation
  - Attached media determines how federated posts are displayed (text/image, etc)
  - Ability to limit and suspend instances, or turn off federation entirely
  - Ability to import, export, and subscribe to blocklists

**Quality of life & Community**
- Accessible / compatible with screen readers
  - Ability to input alt text / image description
  - Optional reminders when alt text / image desc is missing (on by default)
- Granular federation and privacy options on comments, posts, and boards
  - federation
    - federated
    - instance-only
  - privacy
    - public (visible for all)
    - unlisted (visible for all but opted out of discovery)
    - followers (visible to followers)
    - direct (visible to those mentioned)
- Content notices (CWs) for comments, posts, and boards

## Community framework

Currently being discussed!
- vision: defining common goals
- principles: virtues & values to guide contributions and community-building

In the initial phase, regular consultation will take place to build a collective vision for the project and establish a statement of principles, which will then feed into a formal code of conduct.

## Technical framework

To begin, a [prototype](https://codeberg.org/pinetta/proto-pinetta) will be created using Python to encourage contributions and to allow kinks to get worked out. A production version will follow, probably once the prototype comes out of its alpha phase. The production version will use a different language framework, ideally one optimized for performance and concurrency.

### Phases
Requirements for each phase are listed below.

**Pre-alpha:**
- Database schema
- Account creation & log in/out, delete account
- Settings: Notification, privacy/safety settings
- Pins, comments, boards: Create, add/remove, delete, follow and like
  - Pin types: Text, image/GIF, video
- Markdown compatibility for editing
- Federation: Early ActivityPub implementation (specifics TBD), home/local/federated timelines
- Post federation/privacy
- Community safety features: Follow requests, report, ignore, block, ban
- Accessibility: Low animation mode; image description warning

**Alpha phase:**
- Initial public version
- UI improvements, dark mode
- Community safety features: Admin tools, instance block / allow lists
- Board collaboration: Board management, permissions (owner, moderator, contributor), join/leave, invite
- Following/follower management
- Accessibility: High contrast, colourblind modes

**Beta phase:**
- Begin porting to production language (Elixir? Go?)
- Performance review - aim for light resource footprint
- Security review - aim for easy error recovery, airtight data security, 2FA
- Community safety review - focus groups with community members

## Roadmap

### December 2022

**Technical framework:**
- Research & initial decision on language / library framework

**Community framework:**
- Initiate regular consultations on vision & principles

### Jan-Feb 2023

Initial draft: Requirements & scope

Initial dev team assigned

**Technical framework:**
- IDE & toolkit set up

**Community framework:**
- Initial draft: vision & statement of principles

### Mar-Apr 2023

Initial draft: Database schema

**Technical framework:**
- ...

**Community framework:**
- Code of Conduct
- Initial draft: Outline of community features

### May-Jun 2023

...

@<a href="https://fedi.software/@pinetta" rel="me">Pinetta@Fedi.Software</a>